﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ButtonScript : MonoBehaviour
{

    private float Number;
    private float LevelButtonUp;
    private float LevelButtonDown;
    private float levelButtonRight;
    private bool ActiveButton;

    [SerializeField] private Image BackGround;
    [SerializeField] private Image Ground;
    [Header("Button")]
    [SerializeField] private Button UpButton;
    [SerializeField] private Button DownButton;
    [SerializeField] private Button RightButton;
    [SerializeField] private Button BuyButton;

    [Header("Up Button Info")]
    [SerializeField] private Text NameUp;
    [SerializeField] private Text CostUp;
    [SerializeField] private Text CPSUp;
    private float UpdateUp;

    [Header("Down Button Info")]
    [SerializeField] private Text NameDown;
    [SerializeField] private Text CostDown;
    [SerializeField] private Text CPSDown;
    private float UpdateDown;

    [Header("Right Button Info")]
    [SerializeField] private Text CostRight;
    [SerializeField] private Text CPSRight;
    private float UpdateRight;
    [Header("Buy Button Info")]
    [SerializeField] private GameObject ContainerBuy;
    public void DataButton(string nameUp, string nameDown, float number, float levelButtonUp, float levelButtonDown, float leveButtonRight, bool activeButton,Sprite backGround,Sprite ground)
    {
        NameUp.text = nameUp;
        NameDown.text = nameDown;
        Number = number;
        LevelButtonUp = levelButtonUp;
        LevelButtonDown = levelButtonDown;
        levelButtonRight = leveButtonRight;
        ActiveButton = activeButton;
        BackGround.sprite= backGround;
        Ground.sprite = ground;
        ChecActive();
    }
    private void ChecActive()
    {
        if (ActiveButton == true)
        {
            InfoButton();
            InfoSolo();
        }
        else
        {
            ContainerBuy.SetActive(true);
        }
    }
    private void InfoSolo()
    {
        ContainerBuy.SetActive(false);
        UpButton.onClick.AddListener(ButtonUpClick);
        DownButton.onClick.AddListener(ButtonDownClick);
        RightButton.onClick.AddListener(ButtonRightClick);
        BuyButton.onClick.AddListener(ButtonBuyClick);
        
    }
    private void InfoButton()
    {

        UpdateUp = 4 * Mathf.Pow(120, Number);//на сколько увеличивать цену при попупке
        CostUp.text = ((50 * Mathf.Pow(64, Number)) + (UpdateUp * LevelButtonUp)).ToString(); //стартовая цена * на цену при покупке
        CPSUp.text = (14 * Mathf.Pow(64, Number-1)).ToString();//На сколько увеличить CPS

        UpdateDown = 30 * Mathf.Pow(50, Number);//на сколько увеличивать цену при попупке
        CostDown.text = ((200 * Mathf.Pow(64, Number)) + (UpdateDown * LevelButtonDown)).ToString(); //стартовая цена * на цену при покупке
        CPSDown.text = (56 * Mathf.Pow(64, Number - 1)).ToString();//На сколько увеличить CPS

        UpdateRight = 120 * Mathf.Pow(64, Number-1);//на сколько увеличивать цену при попупке
        CostRight.text = ((15 * Mathf.Pow(64, Number)) + (UpdateRight * levelButtonRight)).ToString(); //стартовая цена * на цену при покупке
        CPSRight.text = (4 * Mathf.Pow(58, Number - 1)).ToString();//На сколько увеличить CPS

    } 
    private void ButtonUpClick()
    {
        LevelButtonUp++;
        InfoButton();
    }
    private void ButtonDownClick()
    {
        LevelButtonDown++;
        InfoButton();
    }
    private void ButtonRightClick()
    {
        levelButtonRight++;
        InfoButton();
    }
    private void ButtonBuyClick()
    {
        ActiveButton = true;
        ChecActive();

    }
}
