﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerSpawnLevels : Singleton<ControllerSpawnLevels>
{
    [SerializeField] private RectTransform ContainerSpawn;
    [SerializeField] private GameObject ButtonSpawn;
    private ListModelButton Data;
    public ListModelButton SetData
    {
        set { Data = value;
            SpawnAllButton();
        }
    }
    void Start()
    {
    }
    private void SpawnAllButton()
    {
      
        for (int i = 0; i < Data.ButtonModels.Count; i++)
        {
         var Button =   Instantiate(ButtonSpawn,ContainerSpawn);
            Button.GetComponent<ButtonScript>().DataButton(
                Data.ButtonModels[i].NameUp,
                Data.ButtonModels[i].NameDown,
                Data.ButtonModels[i].NumberLevel,
                Data.ButtonModels[i].LevelButtonUp,
                Data.ButtonModels[i].LevelButtonDown,
                Data.ButtonModels[i].LevelButtonRight,
                Data.ButtonModels[i].IsOpenLevel,
                Data.ButtonModels[i].BackGround,
                Data.ButtonModels[i].Ground
                );
        }
    }
}
