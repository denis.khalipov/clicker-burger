﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameController : Singleton<GameController>
{
    [SerializeField] private Text GoldText;
    private float CurrentGold;
    [SerializeField] private Text CPSText;
    private float CurrensCPS = 1;
    public float SetGold
    {
        set { CurrentGold += value; }
    }
    public float SetCPS
    {
        set { CurrensCPS += value; }
    }
    void Update()
    {
        GoldText.text = CurrentGold.ToString();
        CPSText.text = "+" + CurrensCPS.ToString();
    }
}
