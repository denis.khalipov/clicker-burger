﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[CreateAssetMenu(fileName = "New Fish", menuName = "Fish Data", order = 51)]
public class ScribleContainerFish : ScriptableObject
{
    [SerializeField] private TypesButton Type;
    [SerializeField] private Sprite Image;
    [SerializeField] private int Count;
    [SerializeField] private float Sell;
    [SerializeField] private float Eat;
    [SerializeField] private bool Active;
    [SerializeField] private string Name;

    public Sprite GetImage
    {
        get
        {
            return Image;
        }
    }
    public TypesButton GetTypes
    {
        get { return Type; }
    }
    public int GetCount
    {
        get { return Count; }
    }
    public string GetName
    {
        get { return Name; }
    }
    public float GetSell
    {
        get { return Sell; }
    }
    public float GetEat
    {
        get { return Eat; }
    }

    public bool GetActive
    {
        get { return Active; }
    }


}
