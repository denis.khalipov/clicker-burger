﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class JsonBuilder : Singleton<JsonBuilder>
{

    private ScribleContainerButtonUpdate[] DataButton;
   [SerializeField] private ListModelButton LMF;
    private ModelButton MF;

    public string PathButtonJson;
    private void Awake()
    {
     
    }
    private void Start()
    {
        LoadData();
    }
    public ListModelButton GetButtonData
    {
        get { return LoadButtonData(); }
    }

    public ListModelButton SetSaveButton
    {
        set { SaveButton(value); }
    }
    private void LoadData()
    {

#if UNITY_ANDROID && !UNITY_EDITOR
        pathFish = Path.Combine(Application.persistentDataPath, "SaveButton.json");
#else
        PathButtonJson = Application.persistentDataPath + "/SaveButton.json";
#endif

        if (!File.Exists(PathButtonJson))
        {
            File.Create(PathButtonJson).Dispose();//если нету файла создать файл
            LMF = new ListModelButton() { ButtonModels = new List<ModelButton>() };
            DataButton = DataContainers.Instance.GetDataButton;
            for (int i = 0; i < DataButton.Length; i++)
            {
                ModelButton MF = new ModelButton()
                {
                    Type = DataButton[i].GetTypes,
                    NameUp = DataButton[i].GetNameUp,
                    NameDown = DataButton[i].GetNameDown,
                    NumberLevel = DataButton[i].GetNumberLevel,
                    LevelButtonUp = DataButton[i].GetLevelButtonUp,
                    LevelButtonDown = DataButton[i].GetLevelButtonDown,
                    LevelButtonRight = DataButton[i].GetLevelButtonRight,
                    IsOpenLevel = DataButton[i].GetIsOpenLevel,
                    BackGround = DataButton[i].GetBackGround,
                    Ground = DataButton[i].GetGround
                };
                LMF.ButtonModels.Add(MF);
            }
            SaveButton(LMF);
        }
        LMF = LoadButtonData();
        ControllerSpawnLevels.Instance.SetData = LMF;
        Debug.Log(PathButtonJson);
      //  ControllerSpawnIron.Instance.SpawnFishButton(LMI); // не забыть
        //  SpawnButton();
    }
    private ListModelButton LoadButtonData()
    {
        var jsonData = File.ReadAllText(PathButtonJson);
        return JsonUtility.FromJson<ListModelButton>(jsonData);
    }
    private void SaveButton(ListModelButton _data)
    {
        string dataAsJson = JsonUtility.ToJson(_data, true);
        File.WriteAllText(PathButtonJson, dataAsJson);
    }

}
