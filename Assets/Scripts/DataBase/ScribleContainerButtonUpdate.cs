﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[CreateAssetMenu(fileName = "New Button", menuName = "Button Data", order = 51)]
public class ScribleContainerButtonUpdate : ScriptableObject
{
    [SerializeField] private TypesButton Type;
    [SerializeField] private string NameUp;
    [SerializeField] private string NameDown;
    [SerializeField] private float NumberLevel;
    [SerializeField] private float LevelButtonUp;
    [SerializeField] private float LevelButtonDown;
    [SerializeField] private float LevelButtonRight;
    [SerializeField] private bool IsOpenLevel;
    [SerializeField] private Sprite BackGround;
    [SerializeField] private Sprite Ground;
    public TypesButton GetTypes
    {
        get { return Type; }
    }
    public string GetNameUp
    {
        get { return NameUp; }
    }
    public string GetNameDown
    {
        get { return NameDown; }
    }
    public float GetNumberLevel
    {
        get { return NumberLevel; }
    }
    public float GetLevelButtonUp
    {
        get { return LevelButtonUp; }
    }
    public float GetLevelButtonDown
    {
        get { return LevelButtonDown; }
    }
    public float GetLevelButtonRight
    {
        get { return LevelButtonRight; }
    }
    public bool GetIsOpenLevel
    {
        get { return IsOpenLevel; }
    }
    public Sprite GetBackGround
    {
        get { return BackGround; }
    }
    public Sprite GetGround
    {
        get { return Ground; }
    }
}
